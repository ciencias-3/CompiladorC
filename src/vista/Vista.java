package vista;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class Vista extends JFrame {
	
    GridBagConstraints cons = new GridBagConstraints();

	
	
	public JLabel lblEntrada,lblLexico, lblGenerarCodigos;
	public JTextArea txtEntrada,txtLexico, txtSintactico;
	public JButton  btnLexico, btnSintactico, btnBorrar, btnCerrar, btnCodigoIntermedio, btnAssembler, btnObjeto, btnEjecutable,
					btnEjecutar, btnAbrir, btnGuardar;
	
	String codigoInicial = "int main(){ \n"
			+ "} \n";
	
	public Vista() {
		super("Analizador lexico y sintactico");
		this.getContentPane().setLayout(new GridBagLayout());
  
        
        renderizarTitulos();
        renderizarAreaTextos();
        renderizarBotones();
        
        mostrar();
	}
	
	public void renderizarTitulos() {
		lblEntrada = new JLabel("Entrada");
		lblLexico = new JLabel("Lexico");
		lblGenerarCodigos = new JLabel("Generar codigos");

		//Se setea el tamano de la letra
		lblEntrada.setFont(new Font("Arial", Font.PLAIN, 25));
		lblLexico.setFont(new Font("Arial", Font.PLAIN, 25));
		lblGenerarCodigos.setFont(new Font("Arial", Font.PLAIN, 25));

		cons.weightx = 0.0;
		cons.weighty = 0.0;
		
		cons.gridx = 0;
		cons.gridy = 0;
		cons.gridheight = 1;
		cons.gridwidth = 2;
		this.getContentPane().add(lblEntrada,cons);
		
		
		cons.gridx = 2;
		cons.gridy = 0;
		cons.gridheight = 1;
		cons.gridwidth = 2;
		this.getContentPane().add(lblLexico,cons);
		
		cons.gridx = 0;
		cons.gridy = 5;
		cons.gridheight = 1;
		cons.gridwidth = 4;
		this.getContentPane().add(lblGenerarCodigos,cons);
		
		cons.weightx = 0.0;
		cons.weighty = 0.0;

	}
	
	public void renderizarAreaTextos() {
		
		txtEntrada = new JTextArea(codigoInicial);
		txtLexico = new JTextArea();
		txtSintactico = new JTextArea("Análisis sintáctico no verificado");
		txtSintactico.setForeground(Color.YELLOW);
		txtSintactico.setBackground(Color.DARK_GRAY);
		JScrollPane entrada = new JScrollPane(txtEntrada);
		JScrollPane lexico = new JScrollPane(txtLexico);

		cons.weightx = 1.0;
		cons.weighty = 1.0;
		cons.fill = GridBagConstraints.BOTH;

		
		cons.gridx = 0;
		cons.gridy = 1;
		cons.gridheight = 2;
		cons.gridwidth = 2;
		cons.insets = new Insets(8, 3, 8, 5);
		this.getContentPane().add(entrada,cons);
		
		
		cons.gridx = 2;
		cons.gridy = 1;
		cons.gridheight = 2;
		cons.gridwidth = 2;
		cons.insets = new Insets(8,5,8,3); 
		this.getContentPane().add(lexico,cons);
		
		cons.gridx = 0;
		cons.gridy = 3;
		cons.gridheight = 1;
		cons.gridwidth = 4;
		cons.weighty = 0.0;
		cons.insets = new Insets(8,5,8,3); 
		this.getContentPane().add(txtSintactico,cons);
		
		cons.weightx = 0.0;
		cons.weighty = 0.0;
		cons.fill = GridBagConstraints.NONE;
		cons.insets = new Insets(0,0,0,0); 
		
	}
	
	public void renderizarBotones() {
		
		btnLexico = new JButton("Análisis Lexico");
		btnSintactico = new JButton("Análisis Sintactico");
		btnBorrar = new JButton("Borrar");
		btnCerrar = new JButton("Cerrar");
		btnCodigoIntermedio = new JButton("Intermedio");
		btnAssembler = new JButton("Assembler");
		btnObjeto = new JButton("Objeto");
		btnEjecutable = new JButton("Ejecutable");
		btnEjecutar = new JButton("Ejecutar");
		btnAbrir = new JButton("Cargar");
		btnGuardar = new JButton("Guardar");
		 
		Font fuenteBotones = new Font("Arial", Font.PLAIN, 15);
		btnLexico.setFont(fuenteBotones);
		btnSintactico.setFont(fuenteBotones);		
		btnBorrar.setFont(fuenteBotones);
		btnCerrar.setFont(fuenteBotones);
		btnCodigoIntermedio.setFont(fuenteBotones);
		btnCodigoIntermedio.setEnabled(false);
		btnAssembler.setFont(fuenteBotones);
		btnAssembler.setEnabled(false);
		btnObjeto.setFont(fuenteBotones);
		btnObjeto.setEnabled(false);
		btnEjecutable.setFont(fuenteBotones);
		btnEjecutable.setEnabled(false);
		btnEjecutar.setFont(fuenteBotones);
		btnEjecutar.setEnabled(false);
		btnAbrir.setFont(fuenteBotones);
		btnGuardar.setFont(fuenteBotones);
		btnEjecutar.setEnabled(false);

		cons.weightx = 1.0;
		cons.weighty = 0.0;
		cons.insets = new Insets(10,10,10,10); 
		
		cons.gridx = 0;
		cons.gridy = 4;
		cons.gridheight = 1;
		cons.gridwidth = 1;
		this.getContentPane().add(btnLexico,cons);
		
		cons.gridx = 1;
		cons.gridy = 4;
		cons.gridheight = 1;
		cons.gridwidth = 1;
		this.getContentPane().add(btnSintactico,cons);
		
		cons.gridx = 2;
		cons.gridy = 4;
		cons.gridheight = 1;
		cons.gridwidth = 1;
		this.getContentPane().add(btnBorrar,cons);
		
		cons.gridx = 3;
		cons.gridy = 4;
		cons.gridheight = 1;
		cons.gridwidth = 1;
		this.getContentPane().add(btnCerrar,cons);
		
		cons.gridx = 0;
		cons.gridy = 6;
		cons.gridheight = 1;
		cons.gridwidth = 1;
		this.getContentPane().add(btnCodigoIntermedio,cons);
		
		cons.gridx = 1;
		cons.gridy = 6;
		cons.gridheight = 1;
		cons.gridwidth = 1;
		this.getContentPane().add(btnAssembler,cons);
		
		cons.gridx = 2;
		cons.gridy = 6;
		cons.gridheight = 1;
		cons.gridwidth = 1;
		this.getContentPane().add(btnObjeto, cons);
		
		cons.gridx = 3;
		cons.gridy = 6;
		cons.gridheight = 1;
		cons.gridwidth = 1;
		this.getContentPane().add(btnEjecutable,cons);
		
		cons.gridx = 0;
		cons.gridy = 7;
		cons.gridheight = 1;
		cons.gridwidth = 4;
		this.getContentPane().add(btnEjecutar,cons);
		
		cons.gridx = 5;
		cons.gridy = 1;
		cons.gridheight = 1;
		cons.gridwidth = 1;
		this.getContentPane().add(btnAbrir,cons);
		
		cons.gridx = 5;
		cons.gridy = 2;
		cons.gridheight = 1;
		cons.gridwidth = 1;
		this.getContentPane().add(btnGuardar,cons);
		
		cons.weightx = 0.0;
		cons.weighty = 0.0;
		cons.insets = new Insets(0,0,0,0); 

	}
	
	public void mostrar() {
		this.setDefaultCloseOperation(this.EXIT_ON_CLOSE);
	    this.setPreferredSize(new Dimension(1000, 420));
	    this.pack();
	    this.setLocationRelativeTo(null);
		this.setVisible(true);
	}

}
