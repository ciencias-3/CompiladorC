package vista;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.StringTokenizer;

import javax.swing.JOptionPane;

import analizador.lexico.Lexico;
import analizador.sintactico.*;
import tokens.Token;

public class Logica {
	Vista frame ;
	Logica(){
		frame = new Vista() ;
		setActionListeners();
	}

	public void mostrar() {
		frame.mostrar();
	}

	public void setActionListeners() {
		ActionListener al=new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				if(e.getSource() == frame.btnLexico) {
					analizarLexico();
				}else if(e.getSource() == frame.btnCerrar) {
					System.exit(0); 
				}else if(e.getSource() == frame.btnSintactico) {
					analizarSintactico();
				}else if(e.getSource() == frame.btnCodigoIntermedio) {
					generarCodigoIntermedio();
				}else if(e.getSource() == frame.btnAssembler) {
					generarCodigoAssembler();
				}else if(e.getSource() == frame.btnObjeto) {
					generarCodigoObjeto();
				}else if(e.getSource() == frame.btnEjecutable) {
					generarArchivoEjecutable();
				}else if(e.getSource() == frame.btnEjecutar) {
					ejecutar();
				}else if(e.getSource() == frame.btnAbrir) {
					frame.txtEntrada.setText(cargarArchivo());					
				}else if(e.getSource() == frame.btnGuardar) {
					guardarArchivo(frame.txtEntrada.getText());
				}

			}
		};

		frame.btnBorrar.addActionListener(al);
		frame.btnCerrar.addActionListener(al);
		frame.btnLexico.addActionListener(al);
		frame.btnSintactico.addActionListener(al);
		frame.btnCodigoIntermedio.addActionListener(al);
		frame.btnAssembler.addActionListener(al);
		frame.btnObjeto.addActionListener(al);
		frame.btnEjecutable.addActionListener(al);
		frame.btnEjecutar.addActionListener(al);
		frame.btnAbrir.addActionListener(al);
		frame.btnGuardar.addActionListener(al);
	}

	private void analizarLexico() {
		int linea = 1;

		String codigo = (String) frame.txtEntrada.getText();
		Lexico lexicos = new Lexico(new StringReader(codigo));
		try {
			lexicos.yylex();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String resultado = "LINEA " + linea + "\t\tSIMBOLO\n";
		ArrayList<Token> tokens = lexicos.lstTokens;
		ArrayList<String> simbolos = lexicos.lstSymbolos;
		for(int i = 0 ; i<tokens.size() ; i++) {
			if(tokens.get(i).toString() == "Salto") {
				linea++;
				resultado += "LINEA " + linea + "\t \t SIMBOLO \n";
			}else {
				resultado +=tokens.get(i).getDescripcion();
				resultado += "\t";
				resultado += simbolos.get(i);
				resultado += "\n";
			}
		}

		frame.txtLexico.setText(resultado);
	}


	private void analizarSintactico() {
		String codigo = (String) frame.txtEntrada.getText();
		Lexico lexicos = new Lexico(new StringReader(codigo));
		try {
			lexicos.yylex();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Cantidad tokens: "+lexicos.lstTokens.size());
		Scanner scanner = new Scanner(lexicos.lstTokens);
		Sintaxis s = new Sintaxis(scanner);
		/*try {

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/

		try {
			s.parse();
			//s.debug_parse();
			frame.txtSintactico.setForeground(Color.GREEN);
			frame.txtSintactico.setText("Analisis realizado correctamente");
			frame.btnCodigoIntermedio.setEnabled(true);
		} catch (Exception ex) {
			//Symbol sym = s.getS();
			//frame.txtSintactico.setText("Error de sintaxis. Linea: " + (sym.right + 1) + " Columna: " + (sym.left + 1) + ", Texto: \"" + sym.value + "\"");
			frame.txtSintactico.setForeground(Color.RED);
			frame.txtSintactico.setText(ex.getMessage());
			//txtAnalizarSin.setForeground(Color.red);
		}
	}

	private void generarCodigoIntermedio() {		
		try {
			String[] intermedio = {"cpp", "Programa.cpp", "Programa.i"};
			Process p = Runtime.getRuntime().exec(intermedio);		
			BufferedReader br = new BufferedReader(new InputStreamReader(p.getErrorStream()));
			String r = null;
			while((r = br.readLine()) != null) {
				System.out.println(r);
			}
			System.out.println("Ejecucion del convertidor de código fuente a código intermedio");
			frame.btnAssembler.setEnabled(true);
		} catch (IOException ex) {
			System.out.println(ex);
		}
	}

	private void generarCodigoAssembler() {
		try {
			String[] gassembler = {"g++", "-Wall", "-S", "Programa.i"};
			Process p = Runtime.getRuntime().exec(gassembler);		
			BufferedReader br = new BufferedReader(new InputStreamReader(p.getErrorStream()));
			String r = null;
			while((r = br.readLine()) != null) {
				System.out.println(r);
			}
			System.out.println("Generar codigo assembler");
			frame.btnObjeto.setEnabled(true);
		} catch (IOException ex) {
			System.out.println(ex);
		}
	}

	private void generarCodigoObjeto() {
		try {
			String[] gobjeto = {"as", "Programa.s", "-o", "Programa.o"};
			Runtime.getRuntime().exec(gobjeto);			 
			System.out.println("Generar codigo objeto");
			frame.btnEjecutable.setEnabled(true);
		} catch (IOException ex) {
			System.out.println(ex);
		}
	}

	private void generarArchivoEjecutable() {
		try {
			String[] gejecutable = {"gcc", "Programa.o", "-o", "Programa"};
			Runtime.getRuntime().exec(gejecutable);			
			System.out.println("Generar archivo ejecutable");
			frame.btnEjecutar.setEnabled(true);
		} catch (IOException ex) {
			System.out.println(ex);
		}
	}

	private void ejecutar() {
		try {
			String comando = "./Programa";
			Process p = Runtime.getRuntime().exec(comando);		
			BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream()));
			String r = null, salida = "";
			while((r = br.readLine()) != null) {
				System.out.println(r);
				salida += r;
			}
			System.out.println("Termino de ejecucion del programa en el sistema operativo");
			JOptionPane.showMessageDialog(null, salida);
		} catch (IOException ex) {
			System.out.println(ex);
		}
	}

	private void generarCodigoIntermedioW() {		
		try {
			String[] intermedio = {"intermedio.bat"};
			Runtime.getRuntime().exec(intermedio);			
			System.out.println("Ejecucion del convertidor de código fuente a código intermedio");
			frame.btnAssembler.setEnabled(true);
		} catch (IOException ex) {
			System.out.println(ex);
		}
	}

	private void generarCodigoAssemblerW() {
		try {
			String[] gassembler = {"assembler.bat"};
			Runtime.getRuntime().exec(gassembler);
			System.out.println("Generar codigo assembler");
			frame.btnObjeto.setEnabled(true);
		} catch (IOException ex) {
			System.out.println(ex);
		}
	}

	private void generarCodigoObjetoW() {
		try {
			String[] gobjeto = {"objeto.bat"};
			Runtime.getRuntime().exec(gobjeto);			 
			System.out.println("Generar codigo objeto");
			frame.btnEjecutable.setEnabled(true);
		} catch (IOException ex) {
			System.out.println(ex);
		}
	}

	private void generarArchivoEjecutableW() {
		try {
			String[] gejecutable = {"ejecutable.bat"};
			Runtime.getRuntime().exec(gejecutable);			
			System.out.println("Generar archivo ejecutable");
			frame.btnEjecutar.setEnabled(true);
		} catch (IOException ex) {
			System.out.println(ex);
		}
	}

	private void ejecutarW() {
		try {
			String [] comando={"correr.bat"};
			Runtime.getRuntime().exec(comando);
			System.out.println("Ejetar programa en el sistema operativo");
		} catch (IOException ex) {
			System.out.println(ex);
		}
	}

	private String cargarArchivo() {
		String inputLine, source= "";
	    try {
	          File inFile = new File("Programa.cpp");
	          BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(inFile)));

	          while ((inputLine = br.readLine()) != null) {
	            source += inputLine+"\n";
	          }
	          br.close();
	    } catch (FileNotFoundException ex) {
	      return (null);
	    } catch (IOException ex) {
	      return (null);
	    }
	    return (source);
	}
	
	private void guardarArchivo(String texto) {
		StringTokenizer st = new StringTokenizer(texto, "\n");
		DataOutputStream dos;
		try {
			File outFile = new File("Programa.cpp");		     
			dos = new DataOutputStream(new FileOutputStream(outFile));

			while(st.hasMoreTokens()) {

				dos.writeBytes(st.nextToken()+"\n");
			}
			dos.close();
		} catch (FileNotFoundException ex) {
			System.err.println(ex);
		} catch (IOException ex) {
			System.err.println(ex);
		}
			
	}
}
