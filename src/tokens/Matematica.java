package tokens;

public enum Matematica implements Token {
    Suma("suma"),
    Resta("resta"),
    Resto("resto"),
    Division("division");
    
	private String descripcion;
	private Matematica(String nombre) {
		this.descripcion = "<Operador mat "+nombre+">";
	}
	@Override
	public String getDescripcion() {
		return this.descripcion;
	}
	@Override
    public String toString() {
		return "Matematica"+this.name();
    }
}