package tokens;

public enum Texto implements Token {
	
	Comillas("comillas"), 
	Salto("salto linea"),
	Identificador("identificador"),
	Numero("numero");
	
	private String representacion;
	Texto(String tipo) {
			this.representacion = "<Texto "+tipo+">";
	}
	@Override
	public String getDescripcion() {
		return this.representacion;
	}
	@Override
    public String toString() {
		return "Texto"+this.name();
    }
}