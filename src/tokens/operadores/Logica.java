package tokens.operadores;

import tokens.Token;

public enum Logica implements Token {
    And("And"),
    Or("Or"),
    Negacion("Negacion"),
    AndSolo("And solo"),
    OrSolo("Or solo");
    
	private String descripcion;
	private Logica(String tipo) {
		this.descripcion = "<Op logico "+tipo+">";
	}
	@Override
	public String getDescripcion() {
		return this.descripcion;
	}
	@Override
    public String toString() {
		return "OpLogica"+this.name();
    }
}