package tokens.operadores;

import tokens.Token;

public enum AritmeticosIncrementales implements Token {
	
	Incremento("++"),
	Decremento("--");

	private String descripcion;
	private AritmeticosIncrementales(String tipo) {
		this.descripcion = "<Operador aritmetico incremental "+tipo+">";
	}
	@Override
	public String getDescripcion() {
		return this.descripcion;
	}
	@Override
    public String toString() {
		return "OpAritmeticoIncremental"+this.name();
    }

}
