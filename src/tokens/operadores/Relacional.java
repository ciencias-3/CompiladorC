package tokens.operadores;

import tokens.Token;

public enum Relacional implements Token {
    Mayor("And"),
    Menor("Or"),
    Igual("Igual"),
    Diferente("Diferente"),
    MayorIgual("Mayor o igual"),
    MenorIgual("Menor o igual"),
    SalidaDatos("Salida datos"),
    EntradaDatos("Entrada datos");
    
	private String descripcion;
	private Relacional(String tipo) {
		this.descripcion = "<Op relacional "+tipo+">";
	}
	@Override
	public String getDescripcion() {
		return this.descripcion;
	}
	@Override
    public String toString() {
		return "OpRelacional"+this.name();
    }
}
