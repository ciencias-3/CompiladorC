package tokens.operadores;

import tokens.Token;

public enum Atribucion implements Token {
    Sumar("+="),
    Restar("-="),
    Multiplicar("*="),
    Dividir("/="),
    Resto("%=");
    
	private String descripcion;
	private Atribucion(String tipo) {
		this.descripcion = "<Operador atribucion "+tipo+">";
	}
	@Override
	public String getDescripcion() {
		return this.descripcion;
	}
	@Override
    public String toString() {
		return "OpAtribucion"+this.name();
    }
}