package tokens;

public enum Ciclos implements Token {
    For("for"),
    While("while"),
    Do("do");
    
	private String descripcion;
	private Ciclos(String tipo) {
		this.descripcion = "<Ciclo "+tipo+">";
	}
	@Override
	public String getDescripcion() {
		return this.descripcion;
	}
	@Override
    public String toString() {
		return "Ciclos"+this.name();
    }
}