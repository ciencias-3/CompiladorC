package tokens;

public enum Notacion implements Token {
	
    P_abierto("parentesis abierto"),
    P_cerrado("parentesis cerrado"),
	L_abierta("llave abiera"),
	L_cerrada("llave cerrada"),
	PuntoComa("punto y coma"),
	DosPuntos("dos puntos"),
	C_abierto("corchete abierto"),
	C_cerrado("corchete cerrado"),
	Coma("coma"),
	Punto("punto"),
	Igual("igual"),
	LeerDecimal("d"),
	LeerFlotante("f"),
	LeerDoble("lf"),
	Porcentaje("porcentaje"),
	Numeral("numeral");
	
	private String representacion;
	private Notacion(String tipo) {
		this.representacion = "<Simbolo "+tipo+">";
	}
	@Override
	public String getDescripcion() {
		return this.representacion;
	}
	@Override
    public String toString() {
		return "Notacion"+this.name();
    }
}