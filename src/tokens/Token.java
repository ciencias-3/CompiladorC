package tokens;

public interface  Token {
	public abstract String getDescripcion();
	public abstract String toString();
}
