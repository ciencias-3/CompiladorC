package tokens;

public enum Reservada implements Token {
    Auto("auto"),
	Const("const"),
	Continue("continue"),
	Default("default"),
	Enum("enum"),
	Extern("extern"),
    Goto("goto"),
    Main("main"),
    Register("register"),
    Return("return"),
    Signed("signed"),
    Sizeof("sizeof"),
    Static("static"),
    Struct("struct"),
    Typedef("typedef"),
    Union("union"),
    Unsigned("unsigned"),
    Void("void"),
    Volatile("volatile"),
	Include("include"),
	Scanf("scanf"),
	Printf("printf"),
	Define("define"),
	True("verdadero"),
	False("falso"),
	Cout("cout"),
	Cin("cin"),
	Break("break"),
	Using("using"),
	Namespace("namespace"), 
	Std("std"),
	EOF("eof"); //ESTE NO ES DEL LENGUAJE SINO QUE ES PARA EL SINTACTICO
	//PARA QUE PUEDA IDENTIFICAR EL FINAL
	//VER Scanner.java:38 que devuelve un 0 si no hay tokens 
	//y el Symbolo 0 es EOF, ver sym.java y al ser un simbolo su token debe existir
	private String representacion;
	private Reservada(String tipo) {
		this.representacion = "<Reservada "+tipo+">";
	}
	@Override
	public String getDescripcion() {
		return this.representacion;
	}
	@Override
    public String toString() {
		return "Reservada"+this.name();
    }
}