package tokens;

public enum Decision implements Token {
    If("if"),
    Else("else"),
    Switch("switch"),
    Case("case");
    
	private String descripcion;
	private Decision(String tipo) {
		this.descripcion = "<Decision "+tipo+">";
	}
	@Override
	public String getDescripcion() {
		return this.descripcion;
	}
	@Override
    public String toString() {
		return "Decision"+this.name();
    }
}