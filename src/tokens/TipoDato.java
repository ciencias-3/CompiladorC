package tokens;

import java.util.ArrayList;

public enum TipoDato implements Token {
	Int("int"), 
	Char("char"),
	Long("long"),
	Float("float"),
	Double("double"),
	Byte("byte");
	private String descripcion;
	private TipoDato(String tipo) {
		this.descripcion = "<Tipo de dato "+tipo+">";
	}
	@Override
	public String getDescripcion() {
		// TODO Auto-generated method stub
		return descripcion;
	}
	@Override
    public String toString() {
		return "TipoDato"+this.name();
    }
}
