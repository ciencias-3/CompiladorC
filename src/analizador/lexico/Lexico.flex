package analizador.lexico;
import tokens.*;
import tokens.operadores.*;
import tokens.operadores.Logica;
import java.util.ArrayList;
%%
%public
%class Lexico
%type Token
L=[a-zA-Z_]+
D=[0-9]+
espacio=[ ,\t,\r]+
%{
    public ArrayList<Token> lstTokens = new ArrayList<Token>();
    public ArrayList<String> lstSymbolos = new ArrayList<String>();
    public Token agregarToken(Token token, String simbolo){
    	lstTokens.add(token);
    	lstSymbolos.add(simbolo);
    	return token;
  	}
%}
%%
/* Espacios en blanco */
{espacio} {/*Ignore*/}

/* Comentarios */
( "//"(.)* ) {/*Ignore*/}

/* Salto de linea */
( "\n" ) {/*Ignore*/}

/* Comillas */
( "\"" ) {agregarToken(Texto.Comillas, yytext());}

/* Tipos de datos */
( int) {agregarToken(TipoDato.Int, yytext());}
( char ) {agregarToken(TipoDato.Char, yytext());}
( long ) {agregarToken(TipoDato.Long, yytext());}
( float ) {agregarToken(TipoDato.Float, yytext());}
( byte ) {agregarToken(TipoDato.Byte, yytext());}
( double ) {agregarToken(TipoDato.Double, yytext());}

/*Notacion*/
( "(" ) {agregarToken(Notacion.P_abierto, yytext());}
( ")" ) {agregarToken(Notacion.P_cerrado, yytext());}
( "{" ) {agregarToken(Notacion.L_abierta, yytext());}
( "}" ) {agregarToken(Notacion.L_cerrada, yytext());}
( ";" ) {agregarToken(Notacion.PuntoComa, yytext());}
( ":" ) {agregarToken(Notacion.DosPuntos, yytext());}
( "," ) {agregarToken(Notacion.Coma, yytext());}
( "." ) {agregarToken(Notacion.Punto, yytext());}
( "[" ) {agregarToken(Notacion.C_abierto, yytext());}
( "]" ) {agregarToken(Notacion.C_cerrado, yytext());}
( "=" ) {agregarToken(Notacion.Igual, yytext());}
( "d" ) {agregarToken(Notacion.LeerDecimal, yytext());}
( "f" ) {agregarToken(Notacion.LeerFlotante, yytext());}
( "lf" ) {agregarToken(Notacion.LeerDoble, yytext());}
( "%" ) {agregarToken(Notacion.Porcentaje, yytext());}
( "#" ) {agregarToken(Notacion.Numeral, yytext());}

/*Logica*/
( "&&") {agregarToken(Logica.And, yytext());}
( "||") {agregarToken(Logica.Or, yytext());}
( "!" ) {agregarToken(Logica.Negacion, yytext());}
( "&" ) {agregarToken(Logica.AndSolo, yytext());}
( "|" ) {agregarToken(Logica.OrSolo, yytext());}

/*Operadores Relacionales */
( ">"  ) {agregarToken(Relacional.Mayor, yytext());}
( "<"  ) {agregarToken(Relacional.Menor, yytext());}
( "==" ) {agregarToken(Relacional.Igual, yytext());}
( "!=" ) {agregarToken(Relacional.Diferente, yytext());}
( ">=" ) {agregarToken(Relacional.MayorIgual, yytext());}
( "<=" ) {agregarToken(Relacional.MenorIgual, yytext());}
( "<<" ) {agregarToken(Relacional.SalidaDatos, yytext());}
( ">>" ) {agregarToken(Relacional.EntradaDatos, yytext());}

/* Operadores Atribucion */
( "+=" ) {agregarToken(Atribucion.Sumar, yytext());}
( "-=" ) {agregarToken(Atribucion.Restar, yytext());}
( "*=" ) {agregarToken(Atribucion.Multiplicar, yytext());}
( "/=") {agregarToken(Atribucion.Dividir, yytext());}
( "%=" ) {agregarToken(Atribucion.Resto, yytext());}

/* Operadores aritmeticos incrementales */
( "++" ) {agregarToken(AritmeticosIncrementales.Incremento, yytext());}
( "--" ) {agregarToken(AritmeticosIncrementales.Decremento, yytext());}

/*Decision*/
( "if" ) {agregarToken(Decision.If, yytext());}
( "else" ) {agregarToken(Decision.Else, yytext());}
( "switch" ) {agregarToken(Decision.Switch, yytext());}
( "case" ) {agregarToken(Decision.Case, yytext());}

/*Ciclos*/
( "for" ) {agregarToken(Ciclos.For, yytext());}
( "while" ) {agregarToken(Ciclos.While, yytext());}
( "do" ) {agregarToken(Ciclos.Do, yytext());}


/*Reservada*/
( "main" ) {agregarToken(Reservada.Main, yytext());}
( "auto" ) {agregarToken(Reservada.Auto, yytext());}
( "const" ) {agregarToken(Reservada.Const, yytext());}
( "continue" ) {agregarToken(Reservada.Continue, yytext());}
( "enum" ) {agregarToken(Reservada.Enum, yytext());}
( "extern" ) {agregarToken(Reservada.Extern, yytext());}
( "goto" ) {agregarToken(Reservada.Goto, yytext());}
( "register" ) {agregarToken(Reservada.Register, yytext());}
( "return" ) {agregarToken(Reservada.Return, yytext());}
( "signed" ) {agregarToken(Reservada.Signed, yytext());}
( "sizeof" ) {agregarToken(Reservada.Sizeof, yytext());}
( "static" ) {agregarToken(Reservada.Static, yytext());}
( "struct" ) {agregarToken(Reservada.Struct, yytext());}
( "typedef" ) {agregarToken(Reservada.Typedef, yytext());}
( "union" ) {agregarToken(Reservada.Union, yytext());}
( "unsigned" ) {agregarToken(Reservada.Unsigned, yytext());}
( "void" ) {agregarToken(Reservada.Void, yytext());}
( "volatile" ) {agregarToken(Reservada.Volatile, yytext());}
( "include") {agregarToken(Reservada.Include, yytext());}
( "scanf" ) {agregarToken(Reservada.Scanf, yytext());}
( "printf" ) {agregarToken(Reservada.Printf, yytext());}
( "define" ) {agregarToken(Reservada.Define, yytext());}
( "true" ) {agregarToken(Reservada.True, yytext());}
( "false" ) {agregarToken(Reservada.False, yytext());}
( "cout" ) {agregarToken(Reservada.Cout, yytext());}
( "cin" ) {agregarToken(Reservada.Cin, yytext());}
( "break" ) {agregarToken(Reservada.Break, yytext());}
( "using" ) {agregarToken(Reservada.Using, yytext());}
( "namespace" ) {agregarToken(Reservada.Namespace, yytext());}
( "std" ) {agregarToken(Reservada.Std, yytext());}

/* Identificador */
{L}({L}|{D})* {agregarToken(Texto.Identificador, yytext());}

/* Numero */
("(-"{D}+")")|{D}+ {agregarToken(Texto.Numero, yytext());}

/*Error*/
. {agregarToken(new NoSeEncontro() , yytext());}


