
package analizador.lexico;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Launch {
    public static void main(String[] args) throws Exception {
        String[] camino= {"src/analizador/lexico/Lexico.flex"};
        File myObj = new File("src/analizador/lexico//Lexico.java"); 
        if (myObj.delete()) { 
          System.out.println("Deleted the file: " + myObj.getName());
        } else {
          System.out.println("Failed to delete the file.");
        } 
        generarLex(camino);
    }
    public static void generarLex(String[] camino) throws IOException, Exception {
        jflex.Main.generate(camino);        
    }
    /***************************************************************/
    /**OJO   OJO    OJO   OJO   OJO  OJO   OJO    OJO   OJO   OJO***/
    /***************************************************************/
    /*******UNA VEZ EJECUTADO DAR CLICK DERECHO
     * al paquete analizador.lexico y refrescar
     * sino al ejecutar va a tener en cuenta la version anterior
     * problemas del eclipse :(
     */
    /****************************************************************/
}
