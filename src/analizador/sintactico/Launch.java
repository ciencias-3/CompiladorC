package analizador.sintactico;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import java_cup.internal_error;

public class Launch {

	public static void main(String[] args) throws IOException {
        String[] pathSintaxis= {"-parser","Sintaxis","src/analizador/sintactico/Sintaxis.cup"};

		try {
			java_cup.Main.main(pathSintaxis);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		/*Para que son estos paths?, si se quitan ni sym ni Sintaxis aparecen*/
        Path pathSymbol = Paths.get("src/analizador/sintactico/sym.java");
        if (Files.exists(pathSymbol)) {
            Files.delete(pathSymbol);
        }
        Files.move(
        Paths.get("sym.java"), 
        Paths.get("src/analizador/sintactico/sym.java")
        );
        
        Path pathSintaxis1 = Paths.get("src/analizador/sintactico/Sintaxis.java");
        if (Files.exists(pathSintaxis1)) {
            Files.delete(pathSintaxis1);
        }
        Files.move(
                Paths.get("Sintaxis.java"), 
                Paths.get("src/analizador/sintactico/Sintaxis.java")
        );  

	}

}
