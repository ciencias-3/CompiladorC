package analizador.sintactico;

import java.util.ArrayList;

import java_cup.runtime.Symbol;
import tokens.Token;

public class Scanner implements java_cup.runtime.Scanner{
	private int tokenActual = 0;
	ArrayList<Token> tokens;
	public Scanner(ArrayList<Token> tokens) {
		this.tokens = tokens;
	}
	public int getTokenNumero(Token token) {
		for(int i = 0 ; i< sym.terminalNames.length ; i++) {
			System.out.println("Token: "+token.toString());
			System.out.println(sym.terminalNames[i]);
			if(token.toString().equals(sym.terminalNames[i])) {
				return i;
			}
		}
		return -1;
	}
	public Token getSiguienteToken() {
		if(tokenActual<tokens.size()) {
			Token salida = tokens.get(tokenActual);
			tokenActual++;
			return salida;
		}
		return null;
		
	}
	@Override
	public Symbol next_token() throws Exception {
		Token next = getSiguienteToken();
		if(next != null) {
			Symbol salida = new Symbol(getTokenNumero(next));
			return salida;
		}
		return new Symbol(0);
		
	}

}
